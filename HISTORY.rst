Release History
---------------

0.5.7 (2015-08-03)
++++++++++++++++++

**Improvements**

 - Add ``securepay.LIVE_API_URL`` and ``securepay.TEST_API_URL`` so you don't
   have to define them yourself.


0.5.6 (2015-08-03)
++++++++++++++++++

**Bug fixes**

 - Fixed support for Python 2.7.
 - Enabled tests for Python 2.7, 3.3 and 3.4 with Tox
